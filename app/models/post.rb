class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  validates :title, presence: true, uniqueness: true
  validates_presence_of :body
end
